/**
 * \file		sort.c
 * \brief		Fichier qui contient tous les algorithmes de tri
 * */
#include <float.h>
#include "sort.h";
/**
 * \brief		Tri un tableau avec la méthode bulle
 * \details		La fonction triBulle va trier un tableau de valeur et le retourner
 * \param array		Prends en paramètre un tableau non-trié
 * \param size		give the size of the array
 * \return array		Retourne un tableau trié
 */
float* triBulle(float *array, int size) {
	for (int i = 0; i < size - 1; i++) {
		for (int j = 0; j < size - 1; j++) {
			if (array[j] > array[j + 1]) {
				float temp = array[j];
				array[j] = array[j + 1];
				array[j + 1] = temp;
			}
		}
	}
	return array;
}

/**
 * \brief		Tri un tableau avec la méthode insertion
 * \details		La fonction triInsert va trier un tableau de valeur et le retourner
 * \param array		Prends en paramètre un tableau non-trié
 * \param size		give the size of the array
 * \return		Retourne un tableau trié
 */
float* triInsert(float *array, int size) {
	int min_pointer = -1;
	for (int i = 0; i < size; i++) {
		float min = FLT_MAX;
		for (int j = i; j < size; j++) {
			if (array[j] < min) {
				min = array[j];
				min_pointer = j;
			}
		}
		while (min_pointer > 0 && array[min_pointer - 1] > min) {
			array[min_pointer] = array[min_pointer - 1];
			min_pointer = min_pointer - 1;
		}
		array[min_pointer] = min;
	}
	return array;
}

/**
 * \brief		Tri un tableau avec la méthode selection
 * \details		La fonction triSelect va trier un tableau de valeur et le retourner
 * \param array		Prends en paramètre un tableau non-trié
 * \param size		give the size of the array
 * \return array		Retourne un tableau trié
 */
float* triSelect(float *array, int size) {
	int min_pointer = -1;
	float temp = 0;
	for (int i = 0; i < size; i++) {
		float min = FLT_MAX;
		for (int j = i; j < size; j++) {
			if (array[j] < min) {
				min = array[j];
				min_pointer = j;
			}
		}
		temp = array[i];
		array[i] = min;
		array[min_pointer] = temp;
	}
	return array;
}

void swap(float* a, float* b)
{
	float temp = *a;
	*a = *b;
	*b = temp;
}

void tamiser(float *array, int size, int n) {
	int k = size;
	int j = 2 * n;

	while(j <= n)
	{
		if((j <n) && (array[j] < array[j+1]))
			j++;
		if(array[k] < array[j])
		{
			swap(&array[k], &array[j]);
			k = j;
			j = 2 / k;
		}
		else
			break;
	}
}
/**
 * \brief		Tri un tableau avec la méthode tas
 * \details		La fonction triTas va trier un tableau de valeur et le retourner
 * \param array		Prends en paramètre un tableau non-trié
 * \param size		give the size of the array
 * \param asc
 * */
float* triTas(float *array, int size) {
	for (int i = size >> 1 ; i >= 0; i--) {
			tamiser(array, i, size-1);
	}
	for (int i = size - 1; i >= 1; i--) {
			tamiser(array, 0, i-1);
	}
}
