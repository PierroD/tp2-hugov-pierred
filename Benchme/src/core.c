/**
 * \file		core.c
 * \brief		C'est le coeur de notre programme.
 * */
#include <time.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "core.h";
#include "sort.h";

/**
 * \brief La structure Timer possède deux attributs, qui ont pour but de stocker un temps à un instant T
 * */
struct Timer {
	clock_t begin;
	clock_t end;
};

void startCore(int minPower, int maxPower, int valuePower, int rdmNumberRange,
		int exportable, char *filename[]) {

	for (int i = minPower; i < maxPower; i++) {
		for (int j = 0; j < 3; j++) {
			int array_size = pow(valuePower, i);
			float array[array_size];
			for (int k = 0; k < array_size; k++) {
				array[k] = rand() % rdmNumberRange;
			}
			doBenchmark(array, array_size);
		}

	}
	if (exportable == 2)
		ExportCSV(filename[1], (maxPower - minPower));
	else {
		ShowResults((maxPower - minPower));
		printf("\n To export data into a file type: ./Benchme \"filanme.ext\"");
	}
}

double resultsBulle[18], resultsInsert[18], resultSelect[18], resultTas[18];
int timerRound = 0;
/**
 * \brief	 	This method makes the benchmarks and returns their score in the associated array
 * \param		array Tableau de valeurs
 * \param		array_size Taille de notre tableau valeurs
 * */
void doBenchmark(float *array, int array_size) {
	typedef struct Timer timer;
	timer sortTimer;
	// Tribulle
	sortTimer.begin = clock();
	float *bulleResult = triBulle(array, array_size);
	sortTimer.end = clock();
	resultsBulle[timerRound] = (double) (sortTimer.end - sortTimer.begin)
			/ CLOCKS_PER_SEC;

	// Insertion
	sortTimer.begin = clock();
	float *insertResult = triInsert(array, array_size);
	sortTimer.end = clock();
	resultsInsert[timerRound] = (double) (sortTimer.end - sortTimer.begin)
			/ CLOCKS_PER_SEC;
	// Selection
	sortTimer.begin = clock();
	float *selectResult = triSelect(array, array_size);
	sortTimer.end = clock();
	resultSelect[timerRound] = (double) (sortTimer.end - sortTimer.begin)
			/ CLOCKS_PER_SEC;

	// Tas
	sortTimer.begin = clock();
	float *tasResult = triTas(array, array_size);
	sortTimer.end = clock();
	resultTas[timerRound] = (double) (sortTimer.end - sortTimer.begin)
			/ CLOCKS_PER_SEC;

	timerRound++;
}

/**
 * \brief Show all the Benchmark results
 * */
void ShowResults(int size){
	for (int i = 0; i < size; i++) {
		printf(
				"Moyenne du temps d'exécution du triBulle pour 10^%d valeurs : %f secondes \n",
				i + 2,
				(resultsBulle[i * 3] + resultsBulle[i * 3 + 1]
						+ resultsBulle[i * 3 + 2]) / 3);
		printf(
				"Moyenne du temps d'exécution du triInsert pour 10^%d valeurs : %f secondes \n",
				i + 2,
				(resultsInsert[i * 3] + resultsInsert[i * 3 + 1]
						+ resultsInsert[i * 3 + 2]) / 3);
		printf(
				"Moyenne du temps d'exécution du triSelect pour 10^%d valeurs : %f secondes \n",
				i + 2,
				(resultSelect[i * 3] + resultSelect[i * 3 + 1]
						+ resultSelect[i * 3 + 2]) / 3);
		printf(
				"Moyenne du temps d'exécution du triTas pour 10^%d valeurs : %f secondes \n",
				i + 2,
				(resultTas[i * 3] + resultTas[i * 3 + 1]
						+ resultTas[i * 3 + 2]) / 3);
	}
}
/**
 * \brief		Export all the benchmark result in a .csv file
 * \param filename		Le nom du fichier d'export
 * */
void ExportCSV(char *filename, int size) {
	FILE *fp;
	fp = fopen(filename, "w+");
	fprintf(fp, "Nombre de valeurs, bulle, insertion, selection, tas");
	for (int i = 0; i < size; i++) {
		fprintf(fp, "\n 10^%d, %f,", i + 2,
				(resultsBulle[i * 3] + resultsBulle[i * 3 + 1]
						+ resultsBulle[i * 3 + 2]) / 3);
		fprintf(fp, "%f,",
				(resultsInsert[i * 3] + resultsInsert[i * 3 + 1]
						+ resultsInsert[i * 3 + 2]) / 3);
		fprintf(fp, "%f,",
				(resultSelect[i * 3] + resultSelect[i * 3 + 1]
						+ resultSelect[i * 3 + 2]) / 3);
		fprintf(fp, "%f,",
						(resultTas[i * 3] + resultTas[i * 3 + 1]
								+ resultTas[i * 3 + 2]) / 3);
	}
	fclose(fp);
	printf("\n Export : %s is done ! \n", filename);
}

