/**
 * \file          Benchme.c
 * \author    Hugo VINSON, Pierre DUVEAU
 * \version   1.0
 * \date       08 Octobre 2020
 * \brief      Fait le benchmark de différents algorithmes de tri.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ini.h";
#include "core.h";

/**
 * \brief La structure du fichier de configuration (.ini)
 * */
typedef struct {
	int minPower;
	int maxPower;
	int valuePower;
	int rdmNumberRange;
} configuration;

/**
 * \brief Permet de lire le fichier de configuration (.ini)
 * */
static int handler(void *user, const char *section, const char *name,
		const char *value) {
	configuration *pconfig = (configuration*) user;

#define MATCH(s, n) strcmp(section, s) == 0 && strcmp(name, n) == 0
	if (MATCH("ValueConf", "minPower")) {
		pconfig->minPower = atoi(value);
	} else if (MATCH("ValueConf", "maxPower")) {
		pconfig->maxPower = atoi(value);
	} else if (MATCH("ValueConf", "valuePower")) {
		pconfig->valuePower = atoi(value);
	} else if (MATCH("Limits", "rdmNumberRange")) {
		pconfig->rdmNumberRange = atoi(value);
	} else {
		return 0; /* unknown section/name, error */
	}
	return 1;
}

int main(int argc, char *argv[]) {
	configuration config;
	if (ini_parse("./config.ini", handler, &config) < 0) {
		printf("Can't load 'config.ini'\n");
		return 1;
	} else
		startCore(config.minPower, config.maxPower, config.valuePower,
				config.rdmNumberRange, argc, argv);

}
