# Projet Benchmark


## Objectifs du projet

Dans le cadre d'un cours de développement algorithmique en C, nous avons du comparer le temps d'exécution de plusieurs algorithmes de tri (tri à bulle, tri par sélection, tri par insertion, tri par tas). 
Ensuite, nous avons du exporter ses résultats dans un fichier csv afin de les comparer à l'aide de grapheurs.
Enfin, il a fallut générer une documentation de notre projet à l'aide de doxygen.


## Contraintes

* Les tests effectués pour comparer chaque algorithmes doivent être réalisés avec les mêmes tableaux.
* Chaque algorithme est testé avec 3 tableaux différents.
* Les tableaux utilisés dans les tests contiennent des valeurs comprises entre 0 et 10⁶.
* Les tests sont effectués pour chaque algorithme avec 100, 10³, 10⁴, 10⁵, 10⁶, 10⁷ valeurs.
---

## Commandes

* `make all` (crée le projet, le nettoie et crée la documentation)
* `make documentation` (crée la documenation)
* `make clean` (nettoie le projet)
* `make Benchme` (crée l'arborescende du projet)
* `./Benchme "fichier.csv"` (exporte les données récupérées dans le fichier "fichier.csv")
---

## Résultats attendus

L'algorithme le plus rapide est le tri par tas, ensuite vient le tri par sélection, puis le tri par insertion et enfin le tri bulle.
Pour visualiser le résultat du benchmark vous trouverez un document au format .csv au chemin suivant : <a href="https://gitlab.com/PierroD/tp2-hugov-pierred/-/blob/master/Benchme/Debug/Test.csv">Document.csv</a>
Notez que vous pouvez choisir le nom et l'extension du fichier de rendu lors du lancement du programme (cf. la commande si dessus)

## Evolutions à venir

Nous pourrions ajouter d'autres algorithme de tri afin d'avoir plus de données dans le benchmark pour déterminer quel algorithme de tri est le plus performant.
Effectuer plus de Test sur les saisis que ca ne fasse pas tout planter mais que ca affiche un message d'erreur.
Et faire en sorte que la méthode qui enregistre les moyennes des résultats des algorithmes de tri ait des tableaux de tailles variable (ajourd'hui ils sont en resultTab[18]).

## Mise à jour et Evolution du code
Le projet possède la structure suivante :
```
.
├── Benchme
│   ├── Debug
│   │   ├── Benchme (exécutable)
│   │   ├── config.ini (configuration de l'exécutable)
│   │   ├── Doxyfile (paramètres de la documentation)
│   │   ├── html (possède la documentation générée par doxygen)
│   │   ├── makefile (permet de créer l'exécutable)
│   │   ├── objects.mk
│   │   ├── sources.mk
│   │   ├── src
│   │   │   ├── Benchme.d
│   │   │   ├── Benchme.o
│   │   │   ├── core.d
│   │   │   ├── core.o
│   │   │   ├── sort.d
│   │   │   ├── sort.o
│   │   │   └── subdir.mk
│   │   └── Test.csv
│   └── src (Tous les fichiers du projet)
│       ├── Benchme.c (notre main, permet aussi le lecture du .ini)
│       ├── core.c (le coeur de notre application)
│       ├── core.h
│       ├── ini.c (libraiirie permettant la lecture de .ini)
│       ├── ini.h
│       ├── sort.c (les algorithmes de tri)
│       └── sort.h
└── README.md
```

### Mise à jour de la configuration 
Si vous voulez changer les paramètres afin d'effectuer d'autres tests que ceux présents par défaut dans le 'repos', vous avez juste à éditer le fichier **config.ini** et à relancer le programme avec la commande suivante : `./Benchme "fichier.csv"`

**config.ini** 
```ini
[ValueConf]             ; Configuration pour le tri
minPower=2              ; La taille minimum d'un tableau (en puissance) pour les tests
maxPower=5              ; La taille maximum d'un tableau (en puissance) pour les test
valuePower=10		; La valeur du nombre sur lequel s'applique la puissance

[Limits]                ; Definition des limites
rdmNumberRange=1000000	; Valeur maximum des nombres random qui seront dans les tableau
```

### Ajout d'un ou plusieurs algorithme(s) de tri

Pour ajouter un algorithme de tri, suivez les étapes suivantes :

1. Ajouter l'algorithme de tri dans le fichier sort.c
2. Ajouter son prototype dans le fichier sort.h
3. Ajouter son test dans le fichier core.c
4. Ajouter son rendu dans le fichier core.c
5. Build le projet avec la commande : `make all` ou `make Benchme`
6. Testez votre algorithme de tri pour voir si il bat les autres :smiley:

#### 1. Ajouter l'algorithme de tri dans le fichier sort.c

```c
/**
 * \brief		Tri un tableau avec la méthode nomDuTri
 * \details		La fonction triSelect va trier un tableau de valeur et le retourner
 * \param array		Prends en paramètre un tableau non-trié
 * \param size		give the size of the array
 * \return array		Retourne un tableau trié
 */
float* nomDuTri(float *array, int size) {
	....
	return array;
}

```

#### 2. Ajouter son prototype dans le fichier sort.h

```c
float* nomDuTri(float *array, int size);
```

#### Ajouter son test dans le fichier core.c
```c
double resultsBulle[18], resultsInsert[18], ..., resultNomDuTri[18] ;
void doBenchmark(float *array, int array_size) {
.....
    // NomDuTri
	sortTimer.begin = clock();
	float *nomDuTriResult = triTas(array, array_size);
	sortTimer.end = clock();
	resultNomDuTri[timerRound] = (double) (sortTimer.end - sortTimer.begin)
			/ CLOCKS_PER_SEC;

	timerRound++;
}
```

#### 4. Ajouter son rendu dans le fichier core.c

```c
void ShowResults(int size){
    ....
    printf(
				"Moyenne du temps d'exécution du nomDuTri pour 10^%d valeurs : %f secondes \n",
				i + 2,
				(resultNomDuTri[i * 3] + resultNomDuTri[i * 3 + 1]
						+ resultNomDuTri[i * 3 + 2]) / 3);
}

void ExportCSV(char *filename, int size) {
    fprintf(fp, "Nombre de valeurs, bulle, insertion, selection, tas, nomDuTri");
    ....
    fprintf(fp, "%f,",
						(resultNomDuTri[i * 3] + resultNomDuTri[i * 3 + 1]
								+ resultNomDuTri[i * 3 + 2]) / 3);
}
```

#### 5. Build le projet avec la commande : `make all` ou `make Benchme`
#### 6. Testez votre algorithme de tri pour voir si il bat les autres :smiley: